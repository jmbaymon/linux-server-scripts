#Checks the status of httpd and send to file.log

timestamp=$(date +%H:%M:%S) 


httpStat=$(systemctl status httpd)

dbStat=$(systemctl status mariadb)

echo "----\n
$timestamp + $httpStat + $dbStat\n
----" >> Status.log
 
